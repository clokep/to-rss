# Fabric v2.
https://codeload.github.com/fabric/fabric/zip/2d89b122ed05550c0b4b1fd8836b4a31060ad2a5

# Style checking.
flake8==3.5.0
flake8-builtins==1.1.0      # For disallowing shadowing of Python built-ins.
flake8-import-order==0.17.1
